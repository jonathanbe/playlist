<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

use Illuminate\Http\Request;

Auth::routes();

//Artist
Route::get('/', 'HomeController@index')->name('home');
Route::get('/store', ['middleware' => 'auth', 'uses' => 'ArtistController@create'])->name('create_form');
Route::get('/edit/{id}', ['middleware' => 'auth', 'uses' => 'ArtistController@edit'])->name('edit');
Route::post('/create', ['middleware' => 'auth', 'uses' => 'ArtistController@store'])->name('create');
Route::post('/update', ['middleware' => 'auth', 'uses' => 'ArtistController@update'])->name('update');
Route::delete('/artist/{id}', ['middleware' => 'auth', 'uses' => 'ArtistController@destroy']);

//Album
Route::get('/albums/{artist_id}', ['middleware' => 'auth', 'uses' => 'AlbumController@index' ] )->name('albums_home');
Route::get('/album/store/{artist_id}', ['middleware' => 'auth', 'uses' => 'AlbumController@create' ]);
Route::get('/album/edit/{id}', ['middleware' => 'auth', 'uses' => 'AlbumController@edit'] )->name('albums_edit');
Route::post('/album/create', ['middleware' => 'auth', 'uses' => 'AlbumController@store'])->name('albums_create');
Route::post('/album/update', ['middleware' => 'auth', 'uses' => 'AlbumController@update'])->name('albums_update');
Route::delete('/album/{id}', ['middleware' => 'auth', 'uses' => 'AlbumController@destroy']);
