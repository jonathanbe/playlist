@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>{{$artist->name}} Albums</h1>
        @if(!empty($message))
            <div class="alert alert-success" role="alert">
                {{$message}}
            </div>
        @endif

        <form method="GET" id="search_form">
            <div class="form-group">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <button class="btn btn-outline-secondary" onclick="submit('#seach_form'); return false;" type="button" id="button-addon1">Search</button>
                    </div>
                    <input type="text" name="q" id="q" class="form-control" placeholder="" aria-label="Example text with button addon" aria-describedby="button-addon1">
                </div>
            </div>
        </form>

        <ul class="list-group">
            @foreach($albums as $album)
                <li class="list-group-item d-flex justify-content-between align-items-center">

                    <span id="name-artist-{{$album->id}}">{{$album->name}} - {{$album->year}}</span>

                    <span class="d-flex">
                        <button class="btn btn-info btn-sm mr-1" title="Edit" onclick="window.location='/album/edit/{{ $album->id }}';">
                            <i class="fas fa-edit"></i>
                        </button>

                        <form action="/album/{{$album->id}}" id="delete" title="Delete" method="post">
                            @method('delete')
                            @csrf
                             <button class="btn btn-danger btn-sm">
                                <i class="far fa-trash-alt"></i>
                            </button>
                        </form>
                    </span>
                </li>
            @endforeach
        </ul>
        <a href="/" class="btn btn-dark mt-3">Back</a>
        <a href="/album/store/{{$artist->id}}" class="btn btn-dark mt-3">Add</a>
    </div>
@endsection
