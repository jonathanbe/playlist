@extends('layouts.app')

@section('content')
<div class="container">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form method="POST" action="@if(!empty($album->id)) {{route('albums_update')}} @else {{route('albums_create')}} @endif" id="insert_form">
            @csrf
            <div class="input-group mb-3 col col-4">
                <div class="input-group">
                    @if(!empty($album->id))
                        <input type="hidden" name="id" id="id" value="{{$album->id}}">
                    @endif
                    <input type="hidden" name="artist_id" id="artist_id" value="{{$artist_id}}">
                    <input type="text" name="name" id="name" @if(!empty($album->name)) value="{{$album->name}}" @endif class="form-control" placeholder="Name" aria-label="Name" aria-describedby="button-addon1">
                </div>

                <div class="input-group">
                    <input type="text" name="year" id="year" @if(!empty($album->year)) value="{{$album->year}}" @endif  class="form-control" placeholder="Year" aria-label="Year" aria-describedby="button-addon1">
                </div>
                    <button class="btn btn-outline-secondary mt-3" onclick="window.location='/albums/{{$artist_id}}'; return false;">Back</button>
                <button class="btn btn-outline-secondary mt-3 ml-2"
                        onclick="submit('#insert_form')"
                        type="button"
                        id="button-addon1">
                    Save
                </button>
            </div>
        </form>
</div>
@endsection
