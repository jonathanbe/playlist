@extends('layouts.app')

@section('content')
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="POST" action="@if(!empty($artist->id)) {{route('update')}} @else {{route('create')}} @endif" id="insert_form">
                @csrf
                <div class="input-group mb-3 col col-4">
                    <div class="input-group">
                        @if(!empty($artist->id))
                            <input type="hidden" name="id" id="id" value="{{$artist->id}}">
                        @endif

                        <input type="text" name="name" id="name" @if(!empty($artist->name)) value="{{$artist->name}}" @endif class="form-control" placeholder="Name" aria-label="Name" aria-describedby="button-addon1">
                    </div>

                    <div class="input-group">
                        <input type="text" name="twitter" id="twitter" @if(!empty($artist->twitter)) value="{{$artist->twitter}}" @endif  class="form-control" placeholder="Twitter" aria-label="Twitter" aria-describedby="button-addon1">
                    </div>
                    <button class="btn btn-outline-secondary mt-3" onclick="window.location='/'; return false;">Back</button>
                    <button class="btn btn-outline-secondary mt-3 ml-2"
                            onclick="submit('#insert_form')"
                            type="button"
                            id="button-addon1">
                        Save
                    </button>
                </div>
            </form>
    </div>
@endsection
