@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Artists</h1>
        @if(!empty($message))
            <div class="alert alert-success" role="alert">
                {{$message}}
            </div>
        @endif

        <form method="GET" id="search_form">
            <div class="form-group">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <button class="btn btn-outline-secondary" onclick="submit('#seach_form'); return false;" type="button" id="button-addon1">Search</button>
                    </div>
                    <input type="text" name="q" id="q" class="form-control" placeholder="" aria-label="Example text with button addon" aria-describedby="button-addon1">
                </div>
            </div>
        </form>

        <ul class="list-group">
            @foreach($artists as $artist)
                <li class="list-group-item d-flex justify-content-between align-items-center">

                    <span id="name-artist-{{$artist->id}}">{{$artist->name}}</span>
                    <span class="d-flex">
                        <button class="btn btn-info btn-sm mr-1" title="Edit" onclick="window.location='/edit/{{ $artist->id }}';">
                            <i class="fas fa-edit"></i>
                        </button>

                        <a href="/albums/{{$artist->id}}" title="Albums" class="btn btn-info btn-sm mr-1">
                            <i class="fas fa-external-link-alt"></i>
                        </a>

                        <form action="/artist/{{$artist->id}}" id="delete" title="Delete" method="post">
                            @method('delete')
                            @csrf
                             <button class="btn btn-danger btn-sm">
                                <i class="far fa-trash-alt"></i>
                            </button>
                        </form>
                    </span>
                </li>
            @endforeach
        </ul>
        <a href="{{route('create_form')}}" class="btn btn-dark mt-3">Add</a>
    </div>
@endsection
