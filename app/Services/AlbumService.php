<?php

namespace App\Services;

use App\Artist;
use App\Album;
use Illuminate\Support\Facades\DB;

class AlbumService
{
    public function create($data) : Album
    {
        $album = Album::create($data);

        return $album;
    }

    public function update($data, $id)
    {
        Album::where('id', $id)
            ->update($data);

        return Album::find($id);;
    }

    public function delete($id)
    {
        Album::find($id)->delete();
    }
}
