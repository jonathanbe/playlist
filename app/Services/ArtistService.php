<?php

namespace App\Services;

use App\Artist;
use Illuminate\Support\Facades\DB;

class ArtistService
{
    public function create($data) : Artist
    {
        $artist = Artist::create($data);

        return $artist;
    }

    public function update($data, $id)
    {
        $artist = Artist::where('id', $id)
            ->update($data);

        return $artist;
    }

    public function delete($id)
    {
        Artist::find($id)->delete();
    }
}
