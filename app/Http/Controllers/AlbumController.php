<?php

namespace App\Http\Controllers;

use App\Album;
use App\Artist;
use App\Http\Requests\AlbumFormRequest;
use App\Services\AlbumService;
use Illuminate\Http\Request;

class AlbumController extends Controller
{
    protected $albumService;

    public function __construct(AlbumService $albumService)
    {
        $this->albumService = $albumService;
    }

    public function index(Request $request, $artist_id)
    {
        if(!empty($request->get('q'))){
            $q = $request->get('q');
            $albums = Album::where('artist_id', $artist_id)
                ->where("name", "LIKE", "%$q%")
                ->get();
        }else{
            $albums = Album::where('artist_id', $artist_id)->get();
        }

        $artist = Artist::find($artist_id);
        $message = $request->session() ->get('message');

        return view('album.index', compact('albums', 'artist', 'message'));
    }

    public function create($artist_id)
    {
        return view('album.form', compact('artist_id'));
    }

    public function store(AlbumFormRequest $request)
    {
        $album = $this->albumService->create($request->all());
        $request->session()->flash('message', "Added Album!");

        return redirect("/albums/{$album->artist_id}");
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $album = Album::find($id);
        $artist_id = $album->artist_id;
        return view('album.form', compact('album', 'artist_id'));
    }

    public function update(Request $request)
    {
        $album = $this->albumService->update(
            [
                'name' => $request->get('name'),
                'year' => $request->get('year')
            ],
            $request->get('id')
        );
        $request->session()->flash('message', "Updated Album!");

        return redirect("/albums/{$album->artist_id}");
    }

    public function destroy(Request $request, $id)
    {
        $album = Album::find($id);
        $artist_id = $album->artist_id;

        $this->albumService->delete($id);
        $request->session()->flash('message', "Deleted Artist!");

        return redirect("/albums/{$artist_id}");
    }
}
