<?php

namespace App\Http\Controllers;

use App\Artist;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if(!empty($request->get('q'))){
            $q = $request->get('q');
            $artists = Artist::where("name", "LIKE", "%$q%")->get();
        }else{
            $artists = Artist::all();
        }

        $message = $request->session() ->get('message');

        return view('home', compact('artists', 'message'));
    }
}
