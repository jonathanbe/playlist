<?php

namespace App\Http\Controllers;

use App\Artist;
use App\Http\Requests\ArtistFormRequest;
use App\Services\ArtistService;
use Illuminate\Http\Request;

class ArtistController extends Controller
{
    protected $artistService;

    public function __construct(ArtistService $artistService)
    {
        $this->artistService = $artistService;
    }

    public function index()
    {
        $artists = Artist::all();
        return view('home', ['artists' => $artists]);
    }

    public function create()
    {
        return view('artist.form');
    }

    public function store(ArtistFormRequest $request)
    {
        $this->artistService->create($request->all());
        $request->session()->flash('message', "Added Artist!");

        return redirect('/');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $artist = Artist::find($id);
        return view('artist.form', compact('artist'));
    }

    public function update(Request $request)
    {
        $this->artistService->update(
            [
                'name' => $request->get('name'),
                'twitter' => $request->get('twitter')
            ],
            $request->get('id')
        );
        $request->session()->flash('message', "Updated Artist!");

        return redirect('/');
    }

    public function destroy(Request $request, $id)
    {
        $this->artistService->delete($id);
        $request->session()->flash('message', "Deleted Artist!");

        return redirect('/');
    }
}
